
# 
# wget -qO- https://gitlab.com/ezze-public/backup-handler/-/raw/master/README.txt | bash

apt -y update && apt -y install git
apt -y install mysql-client nano sshpass
git clone https://gitlab.com/ezze-public/backup-handler.git /etc/backupHandler
chmod +x /etc/backupHandler
ln -s /etc/backupHandler/backupHandler /usr/bin/backupHandler

# how does it work
# * backupHandler --dir=/home/dir_one:/home/dir_two --db=my_db:second_db:third_db --skip-gz --sftp=server:port:user:pass:path
# * backupHandler --db=all --ftp=server:user:pass:path
 
# Flags
# --skip-gz : skip gzip compression
# --skip-home : skip making backup of homedir files 
# --skip-db : skip making backup from mysql databases
# --mysqldump-all : dump all mysql databases in .sql files separately [as an alternative backup of mysql]
# --plus-cool-db : backup	all raw	binary database files placed on	/var/lib/mysql [usefull	when the mysql .sql files are not usable]
# --ftp : for remote backup storage, server:user:pass:path
# --sftp : for remote ssh server, server:port:user:pass:path
# --cron : its a cron process, and it will not be interrupted

# Notes
# * it will dump only the sql files lower that 2KB -- ??
# * if --mysqldump-all it will dump all databases
# * if skip-db, mysqldump-all will not work
# * if skip-db and plus-cool-db it will only make the cool-db
# * /backupHandler_lock will be created when a backup process starts, and this file will lock the next backup processes till the current backup process ends.
# * take care about ~/.my.cnf

